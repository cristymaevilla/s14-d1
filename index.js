// -SINGLE LINE COMMENTS to put discription on a specific line (ctrl + /)  

/*
MULTI LINE COMMENTS
to put mutiple lines of comments
(ctrl + shft + /)
(cmd + opt + / for Mac)
*/

// SYNTAX and STATEMENTS

/*
STATEMENTS - instructions that we tell the computer to perform
-usually ends with semicolon (;) but it's NOT required bit it's a good practice to have it

SYNTAX - are set of RULES that describes how statements must be constructed
ex. there must be a period..must end with;..teh arrangement.etc.
*/

console.log("Hello Batch 138!");
/*
"" -when assigning value in words; will look as is 
'' -  */

// VARIABLES
/* used to contain data or values
-make the name of variables more descriptive

HOW TO DECLARE VARIABLES?
-tells our devices  that avariable name is created and is ready to store data
SYNTAX:
let/const variableName

(let) use to handle values that will change over time
(const) constant values e.g.pi value
*/
let myVariable;

console.log(myVariable);

// console.log(hello) hello is not defined, it is na error

// INITIALIZING values to Variables:
/*
-SYNTAX:
	let/const variableName = value;
*/
let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

/*
REASSIGNING VARIABLE VALUES
-changing initial value to another value
-we will NOT use "let" coz it was declared already above (line 46)
*/
productName = 'Laptop';
console.log(productName);

const interest = 3.539;
console.log(interest);
/*you cannot reassign a const value it will be displayed as ERROR*/

let supplier;
supplier = "John Smith Tradings";
console.log(supplier);

let qoute = "He said 'I like pie' and I laughed"
console.log(qoute);


// DECLARING MULTIPLE VARIABLES
// use , to get multiple variables
let productCode = "DC017";
const productBrand = "Dell";

console.log(productCode,productBrand);

// DATA TYPES
/*STRINGS
	-series of characters that create a word,phrase,sentence or any text
	-can be inside ('') or ("")
	-for symbols like , use ""
*/
let country = "Philippines"
let province = 'Metro Manila'

// STRING CONCATENATION
/*
use (+) to combine Multiple string values in one string e.g.
supplier = "John"+ "Smith" +"Tradings";
*/
// Output:Metro Manila, Philippines
let fullAddress = province + ", " + country;
console.log(fullAddress);

// Output: Welcome to Metro Manila, Philippines!
let message = "Welcome to " + province +", " + country + "!"
console.log(message);
// OR
console.log("Welcome to " + fullAddress + "!");


// ESCAPE CHARACTER
/* 
\n to to create a new line like <br> in HTML
\' to use ' inside the ""/''      */
let mailAddress= "Metro Manila \n\nPhilippines";
let message2 = "John's employees went home early";
console.log(message2);
message2 = 'John\'s employees went home early';
console.log(message2);



// A- NUMERIC DATA TYPES:
//1. INTEGERS or whole numbers
let headCount = 26;
console.log(headCount);
// 2. DECIMAL numbers / floating points
let grade = 98.7;
console.log(grade);
// 3. EXPONENTIAL Notation
let planetDistance = 2e10;
console.log(planetDistance);
// COMBINING TEXT and STRINGS
// Output: John's grade last quarter is 98.7.
console.log("John's grade last quarter is "+ grade);


// B- BOOLEAN:
//  1/0 or true/false
let isMarried= true;
let isGoodConduct = false;
console.log("isMarried: "+ isMarried);
console.log("isGoodConduct: "+ isGoodConduct);


// C- ARRAY
/*a special kind of data type that is used to store multiple values
-but must be similar data types
SYNTAX:
   let/const arrayName = [element0, element1, element2, ...];
*/
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);


/* D-OBJECTS:
-used to mimic real world objects or items
-can handle different sets of data values
SYNTAX:
	let/const objectName ={
		propertyA: value,
		propertyB: value
	}
*/
let person = {
	fullName: "Juan Dela Cruz",
	age: 25,
	isMarried: false,
	contact: ["09195678910", "09992828822"],
	address: {
		houseNumber: 458,
		city: "Marikina"
	}
}
console.log(person);


// NULL vs UNDEFINED
/*NULL
- to intetionally express the ABSENCE of a value in a variable declaration
e.g. the value is none
*/
let spouse = null;
let money= 0;
let myName =""; 
/*UNDEFINED
- state of a variable that has been declared but WITHOUT AN ASSIGNED VALUE
-no value in short
*/
let fullName;


/*FUNCTIONS
	- lines or blocks of codes that tells our device to perform a task when called/invoked
DECLARING FUNCTIONS:
	Syntax:
	function functionName(){
		line/block of codes;
	}
*/
/*function printName(){
	console.log("My name is John");
}*/
// INVOKING/ CALLING a function
// printName();

// example: declare a function that will display your fave animal in console log
function favoriteAnimal(){
	console.log("My favorite animal is Bluejay");
}
favoriteAnimal();


// (name) a PARAMETER
// acts a named variable/container that exists only only inside of a function
function printName (name){
	console.log("My name is "+ name);
}
// ARGUMENT- value provided for the function to work properly
printName ("John");
printName("Jane");


function argumentFunction (){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}
invokeFunction(argumentFunction);

// HOW to find more info about a function in console
console.log(argumentFunction);

// using MULTIPLE PARAMETERS
function createFullName(firstName, middleName,lastName){
	console.log("Hello " + firstName +" "+ middleName + " " + lastName)
}
createFullName("Juan", "Pablo", "Cruz");
createFullName("Juan", "Pablo");
createFullName("Juan", "Pablo", "Cruz", "Junior");
// using VARIABLEs as ARGUMENTS:
let firstName="John";
let middleName="Doe";
let lastName="Smith";

createFullName(firstName, middleName, lastName);

// RETURN STATEMENT:
//  stops the execution of function and will be passed to the line/block of code that invoked/called the function 
function returnFullName (firstName, middleName,lastName){
	return firstName+ " "+ middleName+" "+ lastName;
	console.log ("A simple message");
}
let completeName= returnFullName(firstName, middleName, lastName);
console.log(completeName);
// OR
console.log(returnFullName(firstName, middleName, lastName));